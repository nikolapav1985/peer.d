#!/usr/bin/python

"""
FILE check.py

TESTS

INPUT 10 2
OUTPUT False
COMMENT

10 is not power of 2.

INPUT 27 3
OUTPUT True
COMMENT

27 is power of 3 (3^3=27).

INPUT 1 1
OUTPUT True
COMMENT

1 is power of 1 (1^1=1).

INPUT 10 1
OUTPUT False
COMMENT

10 is not power of 1.

INPUT 3 3
OUTPUT True
COMMENT

3 is power of 3 (3^1=3).
"""

def is_divisible(a,b):
    """
        procedure is_divisible

        check if a is divisible by b (if so a should be multiple of b)

        parameters

        a(integer) first number (could be multiple of b)
        b(integer) second number (could be divisor of a)

        returns

        check(boolean) true if a is divisible by b, false in other case
    """
    if a%b == 0:
        return True
    else:
        return False

def is_power(a,b):
    """
        procedure is_power

        check if a is power of b

        condition, a is power of b if a is divisible by b and (a/b) is power of b

        parameters

        a(integer) first number (could be multiple of b)
        b(integer) second number (could be divisor of a)

        returns

        check(boolean) true if a is power of b, false in other case
    """
    if a == b:
        return True
    elif b == 1:
        return False
    else:
        return is_divisible(a,b) and is_power(a/b,b)

if __name__=="__main__":
    print("is_power({0},{1}) returns: {2} ".format(10,2,is_power(10,2)))
    print("is_power({0},{1}) returns: {2} ".format(27,3,is_power(27,3)))
    print("is_power({0},{1}) returns: {2} ".format(1,1,is_power(1,1)))
    print("is_power({0},{1}) returns: {2} ".format(10,1,is_power(10,1)))
    print("is_power({0},{1}) returns: {2} ".format(3,3,is_power(3,3)))
