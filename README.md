Programming assignment unit 4
-----------------------------

- implement procedure is_divisible (check if one number is divisible by other)
- implement procedure is_power (check if one number is power of other)

Run test
--------

Type ./check.py

Description
-----------

- Description is found in file check.py
- Test output is found in out.txt

Test environment
----------------

OS lubuntu 16.04 lts 64 bit kernel version 4.13.0 python version 2.7.12
